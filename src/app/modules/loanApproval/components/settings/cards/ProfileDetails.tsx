import React, { useState } from "react";
import { toAbsoluteUrl } from "../../../../../../_metronic/helpers";
import {
  IProfileDetails,
  profileDetailsInitValues as initialValues
} from "../SettingsModel";
import * as Yup from "yup";
import { useFormik } from "formik";
import { SignInMethod } from "./SignInMethod";
import axios from "axios";

const ageGroup = [
  {
    age: "18-24",
    value: 0
  },
  {
    age: "25-29",
    value: 1
  },
  {
    age: "30-34",
    value: 2
  },
  {
    age: "35-39",
    value: 3
  },
  {
    age: "40-44",
    value: 4
  },
  {
    age: "45-49",
    value: 5
  },
  {
    age: "50-54",
    value: 6
  },
  {
    age: "55-59",
    value: 7
  },
  {
    age: "60-64",
    value: 8
  },
  {
    age: "65-69",
    value: 9
  },
  {
    age: "70-74",
    value: 10
  },
  {
    age: "75-79",
    value: 11
  },
  {
    age: "80 or older",
    value: 12
  }
];

const ethnicityGroup = [
  {
    ethnicity: "American Indian/Alaskan Native",
    value: 0
  },
  {
    ethnicity: "Asian",
    value: 1
  },
  {
    ethnicity: "Black",
    value: 2
  },
  {
    ethnicity: "Hispanic",
    value: 3
  },
  {
    ethnicity: "Other",
    value: 4
  },
  {
    ethnicity: "White",
    value: 5
  }
];

const profileDetailsSchema = Yup.object().shape({
  age: Yup.string().required("age is required"),
  experience: Yup.string().required("experience is required"),
  income: Yup.string().required("income is required"),
  zip: Yup.string().required("zip is required"),
  family: Yup.string().required("family is required"),
  cCAvg: Yup.string().required("cCAvg is required"),
  education: Yup.string().required("education is required"),
  mortgage: Yup.string().required("mortgage is required"),
  sec: Yup.string().required("sec is required"),
  cd: Yup.string().required("cd is required"),
  online: Yup.string().required("online is required"),
  credit: Yup.string().required("credit is required")
});

const ProfileDetails: React.FC = () => {
  const [data, setData] = useState<IProfileDetails>(initialValues);
  const updateData = (fieldsToUpdate: Partial<IProfileDetails>): void => {
    const updatedData = Object.assign(data, fieldsToUpdate);
    setData(updatedData);
  };
  const [result, setResult] = useState('')
  const [loading, setLoading] = useState(false);
  const formik = useFormik<IProfileDetails>({
    initialValues,
    validationSchema: profileDetailsSchema,
    onSubmit: async (values) => {
      setLoading(true);
      console.log(values)
      // const updatedData = Object.assign(data, values);
      // console.log(updatedData);
      const res = await axios.post('http://192.241.139.130:7000/loan-approval', values, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      setResult(res.data);
      setLoading(false);
    }
  });

  return (
    <>
      <div className="card mb-5 mb-xl-10">
        <div
          className="card-header border-0 cursor-pointer"
          role="button"
          data-bs-toggle="collapse"
          data-bs-target="#kt_account_profile_details"
          aria-expanded="true"
          aria-controls="kt_account_profile_details"
        >
          <div className="card-title m-0">
            <h3 className="fw-bolder m-0">Prameters</h3>
          </div>
        </div>

        <div id="kt_account_profile_details" className="collapse show">
          <form onSubmit={formik.handleSubmit} noValidate className="form">
            <div className="card-body border-top p-9">
              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Age
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("age")}
                  />
                  {formik.touched.age && formik.errors.age && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">{formik.errors.age}</div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Experience
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("experience")}
                  />
                  {formik.touched.experience && formik.errors.experience && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.experience}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Income
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("income")}
                  />
                  {formik.touched.income && formik.errors.income && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.income}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Zip
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("zip")}
                  />
                  {formik.touched.zip && formik.errors.zip && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">{formik.errors.zip}</div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Family
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("family")}
                  />
                  {formik.touched.family && formik.errors.family && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.family}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  CCAvg
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("cCAvg")}
                  />
                  {formik.touched.cCAvg && formik.errors.cCAvg && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">{formik.errors.cCAvg}</div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Education
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("education")}
                  />
                  {formik.touched.education && formik.errors.education && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.education}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Mortgage
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("mortgage")}
                  />
                  {formik.touched.mortgage && formik.errors.mortgage && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.mortgage}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Sec
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("sec")}
                  />
                  {formik.touched.sec && formik.errors.sec && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">{formik.errors.sec}</div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Cd
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("cd")}
                  />
                  {formik.touched.cd && formik.errors.cd && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">{formik.errors.cd}</div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Online
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("online")}
                  />
                  {formik.touched.online && formik.errors.online && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.online}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Credit
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("credit")}
                  />
                  {formik.touched.credit && formik.errors.credit && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.credit}
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>

            <div className="card-footer d-flex justify-content-end py-6 px-9">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={loading}
              >
                {!loading && "Predict"}
                {loading && (
                  <span
                    className="indicator-progress"
                    style={{ display: "block" }}
                  >
                    Please wait...{" "}
                    <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                  </span>
                )}
              </button>
            </div>
          </form>
        </div>
      </div>
      <SignInMethod res={result} />
    </>
  );
};

export { ProfileDetails };
