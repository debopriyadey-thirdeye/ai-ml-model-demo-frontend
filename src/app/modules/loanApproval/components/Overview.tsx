/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Link } from "react-router-dom";
import { KTIcon } from "../../../../_metronic/helpers";
import {
  ChartsWidget1,
  ListsWidget5,
  TablesWidget1,
  TablesWidget5
} from "../../../../_metronic/partials/widgets";

export function Overview() {
  return (
    <>
      <div className="card mb-5 mb-xl-10" id="kt_profile_details_view">
        <div className="card-header cursor-pointer">
          <div className="card-title m-0">
            <h3 className="fw-bolder m-0">Model Details</h3>
          </div>

          <Link
            to="/crafted/account/settings"
            className="btn btn-primary align-self-center"
          >
            Predict
          </Link>
        </div>

        <div className="card-body p-9">
          {/* <div className='row mb-7'>
            <label className='col-lg-4 fw-bold text-muted'>Full Name</label>

            <div className='col-lg-8'>
              <span className='fw-bolder fs-6 text-dark'>Max Smith</span>
            </div>
          </div>

          <div className='row mb-7'>
            <label className='col-lg-4 fw-bold text-muted'>Company</label>

            <div className='col-lg-8 fv-row'>
              <span className='fw-bold fs-6'>Keenthemes</span>
            </div>
          </div>

          <div className='row mb-7'>
            <label className='col-lg-4 fw-bold text-muted'>
              Contact Phone
              <i
                className='fas fa-exclamation-circle ms-1 fs-7'
                data-bs-toggle='tooltip'
                title='Phone number must be active'
              ></i>
            </label>

            <div className='col-lg-8 d-flex align-items-center'>
              <span className='fw-bolder fs-6 me-2'>044 3276 454 935</span>

              <span className='badge badge-success'>Verified</span>
            </div>
          </div>

          <div className='row mb-7'>
            <label className='col-lg-4 fw-bold text-muted'>Company Site</label>

            <div className='col-lg-8'>
              <a href='#' className='fw-bold fs-6 text-dark text-hover-primary'>
                keenthemes.com
              </a>
            </div>
          </div>

          <div className='row mb-7'>
            <label className='col-lg-4 fw-bold text-muted'>
              Country
              <i
                className='fas fa-exclamation-circle ms-1 fs-7'
                data-bs-toggle='tooltip'
                title='Country of origination'
              ></i>
            </label>

            <div className='col-lg-8'>
              <span className='fw-bolder fs-6 text-dark'>Germany</span>
            </div>
          </div>

          <div className='row mb-7'>
            <label className='col-lg-4 fw-bold text-muted'>Communication</label>

            <div className='col-lg-8'>
              <span className='fw-bolder fs-6 text-dark'>Email, Phone</span>
            </div>
          </div>

          <div className='row mb-10'>
            <label className='col-lg-4 fw-bold text-muted'>Allow Changes</label>

            <div className='col-lg-8'>
              <span className='fw-bold fs-6'>Yes</span>
            </div>
          </div> */}

          <div className="notice d-flex bg-light-info rounded border-info border border-dashed p-6 my-4">
            {/* <KTIcon
              iconName="information-5"
              className="fs-2tx text-info me-4"
            /> */}
            <div className="d-flex flex-stack flex-grow-1">
              <div className="fw-bold">
                <h4 className="text-gray-800 fw-bolder">
                  Additional Information
                </h4>
                <div className="fs-6 text-gray-600">
                  An Artificial neural network is usually a computational
                  network based on biological neural networks that construct the
                  structure of the human brain. It also has neurons that are
                  linked to each other in various layers of the networks. These
                  neurons are known as nodes. In order to define a neural
                  network that consists of a large number of artificial neurons,
                  which are termed units arranged in a sequence of layers.
                  {/* <Link className="fw-bolder" to="/crafted/account/settings">
                    {" "}
                    Add Payment Method
                  </Link> */}
                  .
                </div>
              </div>
            </div>
          </div>
          <div className="notice d-flex bg-light-success rounded border-success border border-dashed p-6 my-4">
            {/* <KTIcon
              iconName="information-5"
              className="fs-2tx text-success me-4"
            /> */}
            <div className="d-flex flex-stack flex-grow-1">
              <div className="fw-bold">
                <h4 className="text-gray-800 fw-bolder">Uses</h4>
                <div className="fs-6 text-gray-600">
                  In this project, a neural network model with one hidden layer
                  was trained to identify the customer segments that are
                  eligible for loan amounts so that the business can
                  specifically target these customers.
                  {/* <Link className="fw-bolder" to="/crafted/account/settings">
                    {" "}
                    Add Payment Method
                  </Link> */}
                  .
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <div className='row gy-10 gx-xl-10'>
        <div className='col-xl-6'>
          <ChartsWidget1 className='card-xxl-stretch mb-5 mb-xl-10' />
        </div>

        <div className='col-xl-6'>
          <TablesWidget1 className='card-xxl-stretch mb-5 mb-xl-10' />
        </div>
      </div> */}

      {/* <div className='row gy-10 gx-xl-10'>
        <div className='col-xl-6'>
          <ListsWidget5 className='card-xxl-stretch mb-5 mb-xl-10' />
        </div>

        <div className='col-xl-6'>
          <TablesWidget5 className='card-xxl-stretch mb-5 mb-xl-10' />
        </div>
      </div> */}
    </>
  );
}
