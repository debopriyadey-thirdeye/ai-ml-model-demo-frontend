import React from "react";
import { Navigate, Route, Routes, Outlet } from "react-router-dom";
import { PageLink, PageTitle } from "../../../_metronic/layout/core";
import { Overview } from "./components/Overview";
import { Settings } from "./components/settings/Settings";
import { AccountHeader } from "./AccountHeader";
import { Logout, AuthPage, useAuth } from "../../modules/auth";

const accountBreadCrumbs: Array<PageLink> = [
  {
    title: "LoanApproval",
    path: "/crafted/loanApproval/overview",
    isSeparator: false,
    isActive: false
  },
  {
    title: "",
    path: "",
    isSeparator: true,
    isActive: false
  }
];

const LoanApprovalPage: React.FC = () => {
  const { currentUser } = useAuth();

  return (
    <Routes>
      <Route
        element={
          <>
            <AccountHeader />
            <Outlet />
          </>
        }
      >
        <Route
          path="overview"
          element={
            <>
              <PageTitle breadcrumbs={accountBreadCrumbs}>Overview</PageTitle>
              <Overview />
            </>
          }
        />
        {currentUser ? (
          <Route
            path="settings"
            element={
              <>
                <PageTitle breadcrumbs={accountBreadCrumbs}>Settings</PageTitle>
                <Settings />
              </>
            }
          />
        ) : (
          <>
            <Route path="auth/*" element={<AuthPage />} />
            <Route path="*" element={<Navigate to="/auth" />} />
          </>
        )}

        <Route
          index
          element={<Navigate to="/crafted/loanApproval/overview" />}
        />
      </Route>
    </Routes>
  );
};

export default LoanApprovalPage;
