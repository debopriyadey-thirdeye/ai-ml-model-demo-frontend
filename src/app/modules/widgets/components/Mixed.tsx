import React, { FC, useEffect } from "react";
import {
  MixedWidget1a,
  MixedWidget1b,
  MixedWidget1c,
  MixedWidget2,
  MixedWidget3a,
  MixedWidget3b,
  MixedWidget3c,
  MixedWidget4,
  MixedWidget5,
  MixedWidget6,
  MixedWidget7,
  MixedWidget8,
  MixedWidget9,
  MixedWidget10,
  MixedWidget11
} from "../../../../_metronic/partials/widgets";

const Mixed: FC = () => {

  useEffect(() => {
    // api call
  }, [])
  

  return (
    <>
      {/* begin::Row */}
      <div className="row g-5 g-xl-8">
        {/* begin::Col */}
        <div className="col-xl-4">
          <MixedWidget1a className="card-xl-stretch mb-xl-8" color="primary" />
        </div>
        {/* end::Col */}

        {/* begin::Col */}
        <div className="col-xl-4">
          <MixedWidget1b className="card-xl-stretch mb-xl-8" color="danger" />
        </div>
        {/* end::Col */}

        {/* begin::Col */}
        <div className="col-xl-4">
          <MixedWidget1c
            className="card-xl-stretch mb-5 mb-xl-8"
            color="success"
          />
        </div>
        {/* end::Col */}
      </div>
      {/* end::Row */}

      {/* begin::Row */}

      {/* end::Row */}

      {/* begin::Row */}
      <div className="row g-5 g-xl-8">
        {/* begin::Col */}
        <div className="col-xl-4">
          <MixedWidget3a
            className="card-xl-stretch mb-xl-8"
            chartColor="info"
            chartHeight="250px"
          />
        </div>
        {/* end::Col */}

        {/* begin::Col */}
        <div className="col-xl-4">
          <MixedWidget3b
            className="card-xl-stretch mb-xl-8"
            chartColor="danger"
            chartHeight="250px"
          />
        </div>
        {/* end::Col */}

        {/* begin::Col */}
        <div className="col-xl-4">
          <MixedWidget3c
            className="card-xl-stretch mb-5 mb-xl-8"
            chartColor="primary"
            chartHeight="250px"
          />
        </div>
        {/* end::Col */}
      </div>
      {/* end::Row */}

      {/* begin::Row */}
      <div className="row g-5 g-xl-4">
        {/* begin::Col */}
        <div className="col-xl-4">
          <MixedWidget7
            heading="Heart Disease Detection"
            className="card-xl-stretch mb-xl-8"
            chartColor="primary"
            chartHeight="200px"
            param={84}
          />
        </div>
        {/* end::Col */}

        {/* begin::Col */}
        <div className="col-xl-4">
          <MixedWidget7
            heading="Text Classifier"
            className="card-xl-stretch mb-xl-8"
            chartColor="success"
            chartHeight="200px"
            param={92}
          />
        </div>
        {/* end::Col */}

        {/* begin::Col */}
        <div className="col-xl-4">
          <MixedWidget7
            heading="Pole Crossarm Detection"
            className="card-xl-stretch mb-xl-8"
            chartColor="info"
            chartHeight="200px"
            param={90}
          />
        </div>
        {/* end::Col */}
      </div>
      {/* end::Row */}
      <div className="row g-5 g-xl-8">
        {/* begin::Col */}
        <div className="col-xl-12">
          <MixedWidget8
            className="card-xl-stretch mb-xl-8"
            chartColor="primary"
            chartHeight="150px"
          />
        </div>
        {/* end::Col */}

        {/* begin::Col */}
        {/* <div className='col-xl-4'>
          <MixedWidget8
            className='card-xl-stretch mb-xl-8'
            chartColor='success'
            chartHeight='150px'
          />
        </div> */}
        {/* end::Col */}

        {/* begin::Col */}
        {/* <div className='col-xl-4'>
          <MixedWidget8
            className='card-xl-stretch mb-5 mb-xl-8'
            chartColor='danger'
            chartHeight='150px'
          />
        </div> */}
        {/* end::Col */}
      </div>
      {/* end::Row */}
    </>
  );
};

export { Mixed };
