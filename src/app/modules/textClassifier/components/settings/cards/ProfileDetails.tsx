import React, { useState } from "react";
import { toAbsoluteUrl } from "../../../../../../_metronic/helpers";
import {
  IProfileDetails,
  profileDetailsInitValues as initialValues
} from "../SettingsModel";
import * as Yup from "yup";
import { useFormik } from "formik";
import { SignInMethod } from "./SignInMethod";
import axios from "axios";

const profileDetailsSchema = Yup.object().shape({
  comment_text: Yup.string().required("Text is required")
});

const ProfileDetails: React.FC = () => {
  const [data, setData] = useState<IProfileDetails>(initialValues);
  const updateData = (fieldsToUpdate: Partial<IProfileDetails>): void => {
    const updatedData = Object.assign(data, fieldsToUpdate);
    setData(updatedData);
  };
  const [result, setResult] = useState([""]);
  const [loading, setLoading] = useState(false);

  const [url, setUrl] = useState("");

  const formik = useFormik<IProfileDetails>({
    initialValues,
    validationSchema: profileDetailsSchema,
    onSubmit: async (values) => {
      setLoading(true);
      console.log(values.comment_text);
      const updatedData = Object.assign(data, values);
      console.log(updatedData);
      // const res = await axios.post(
      //   "https://iris-cloud-run-service-wpikrhxudq-uc.a.run.app/classify",
      //   { data:  '[[5.1, 3.5, 1.4, 0.2]]' },
      //   {
      //     headers: {
      //       "Content-Type": "application/json"
      //     }
      //   }
      // );
      // console.log(res.data);

      setTimeout(() => {
        setResult(["0", "0", "0", "0", "0", "0"]);
        setLoading(false);
      }, 1000);
    }
  });

  return (
    <>
      {/* <div className="card mb-5 mb-xl-10">
        <div
          className="card-header border-0 cursor-pointer"
          role="button"
          data-bs-toggle="collapse"
          data-bs-target="#kt_account_profile_details"
          aria-expanded="true"
          aria-controls="kt_account_profile_details"
        >
          <div className="card-title m-0">
            <h3 className="fw-bolder m-0">Requirement</h3>
          </div>
        </div>

        <div id="kt_account_profile_details" className="collapse show">
          <form noValidate className="form">
            <div className="card-body border-top p-9">
              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  Hosted URL
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="text"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="Ender Generated URL"
                    value={url}
                    onChange={(e) => {
                      setUrl(e.target.value);
                    }}
                  />
                </div>
              </div>
            </div>
          </form>
        </div>
      </div> */}

      
        <div className="card mb-5 mb-xl-10">
          <div
            className="card-header border-0 cursor-pointer"
            role="button"
            data-bs-toggle="collapse"
            data-bs-target="#kt_account_profile_details"
            aria-expanded="true"
            aria-controls="kt_account_profile_details"
          >
            <div className="card-title m-0">
              <h3 className="fw-bolder m-0">Prameters</h3>
            </div>
          </div>

          <div id="kt_account_profile_details" className="collapse show">
            <form onSubmit={formik.handleSubmit} noValidate className="form">
              <div className="card-body border-top p-9">
                <div className="row mb-6">
                  <label className="col-lg-4 col-form-label required fw-bold fs-6">
                    Text
                  </label>

                  <div className="col-lg-8 fv-row">
                    <input
                      type="text"
                      className="form-control form-control-lg form-control-solid"
                      placeholder="Enter Your Text Here"
                      {...formik.getFieldProps("comment_text")}
                    />
                    {formik.touched.comment_text && formik.errors.comment_text && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.comment_text}
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="card-footer d-flex justify-content-end py-6 px-9">
                <button
                  type="submit"
                  className="btn btn-primary"
                  disabled={loading}
                >
                  {!loading && "Predict"}
                  {loading && (
                    <span
                      className="indicator-progress"
                      style={{ display: "block" }}
                    >
                      Please wait...{" "}
                      <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                    </span>
                  )}
                </button>
              </div>
            </form>
          </div>
        </div>
      
      {result.length > 1 && <SignInMethod res={result} />}
    </>
  );
};

export { ProfileDetails };
