import { useMemo } from "react";
import { useTable, ColumnInstance, Row } from "react-table";
import { CustomHeaderColumn } from "../table/columns/CustomHeaderColumn";
import { CustomRow } from "../table/columns/CustomRow";
import {
  useQueryResponseData,
  useQueryResponseLoading
} from "../core/QueryResponseProvider";
import { usersColumns } from "./columns/_columns";
import { User } from "../core/_models";
import { UsersListLoading } from "../components/loading/UsersListLoading";
import { UsersListPagination } from "../components/pagination/UsersListPagination";
import { KTCardBody } from "../../../../../../_metronic/helpers";

const assetData = [
  {
    id: 1,
    name: "Train_Text_Data",
    avatar: "avatars/300-6.jpg",
    email: "smith@kpmg.com",
    position: "tabular",
    role: "Tabular",
    last_login: "Cloud storage",
    two_steps: "Raw",
    joined_day: "15 April 2023, 9:23 pm",
    online: false
  },
  {
    id: 3,
    name: "Object_Detection",
    avatar: "avatars/300-1.jpg",
    email: "max@kt.com",
    position: "Software Enginer",
    role: "Image",
    last_login: "Local",
    two_steps: "Staging",
    joined_day: "10 April 2023, 9:23 pm",
    online: false
  },
  {
    id: 4,
    name: "Text_Comment_Classifier",
    avatar: "avatars/300-5.jpg",
    email: "sean@dellito.com",
    position: "Web Developer",
    role: "Tabular",
    last_login: "S3",
    two_steps: "Prod",
    joined_day: "12 April 2023, 9:23 pm",
    online: false
  }
]

const UsersTable = () => {
  const users = useQueryResponseData();
  const isLoading = useQueryResponseLoading();
  const data = useMemo(() => assetData, [users]);
  const columns = useMemo(() => usersColumns, []);
  const { getTableProps, getTableBodyProps, headers, rows, prepareRow } = useTable({
    columns,
    data
  });

  return (
    <KTCardBody className="py-4">
      <div className="table-responsive">
        <table
          id="kt_table_users"
          className="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
          {...getTableProps()}
        >
          <thead>
            <tr className="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
              {headers.map((column: ColumnInstance<User>) => (
                <CustomHeaderColumn key={column.id} column={column} />
              ))}
            </tr>
          </thead>
          <tbody className="text-gray-600 fw-bold" {...getTableBodyProps()}>
            {rows.length > 0 ? (
              rows.map((row: Row<User>, i) => {
                prepareRow(row);
                return <CustomRow row={row} key={`row-${i}-${row.id}`} />;
              })
            ) : (
              <tr>
                <td colSpan={7}>
                  <div className="d-flex text-center w-100 align-content-center justify-content-center">
                    No matching records found
                  </div>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
      {/* <UsersListPagination /> */}
      {isLoading && <UsersListLoading />}
    </KTCardBody>
  );
};

export { UsersTable };
