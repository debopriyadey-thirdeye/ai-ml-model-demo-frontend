import {FC} from 'react'

type Props = {
  two_steps?: string
}

const UserTwoStepsCell: FC<Props> = ({two_steps}) => (
  <> {two_steps == "Prod" ? (<div className='badge badge-light-success fw-bolder'>Prod</div>) : two_steps === "Raw" ? (<div className='badge badge-light-danger fw-bolder'>Raw</div>) : (<div className='badge badge-light-info fw-bolder'>Staging</div>)}</>
)

export {UserTwoStepsCell}
