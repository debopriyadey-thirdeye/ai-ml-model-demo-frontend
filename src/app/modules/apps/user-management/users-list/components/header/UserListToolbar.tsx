import {KTIcon} from '../../../../../../../_metronic/helpers'
import {useListView} from '../../core/ListViewProvider'
import UserAddModal from '../../user-edit-modal/UserAddModal'
import {UsersListFilter} from './UsersListFilter'

const UsersListToolbar = () => {
  const {setItemIdForUpdate, setForAdd} = useListView()
  const openAddUserModal = () => {
    setItemIdForUpdate(undefined)
    setForAdd(null)
  }

  return (
    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
      <UsersListFilter />

      {/* begin::Export */}
      <button type='button' className='btn btn-light-primary me-3'>
        <KTIcon iconName='exit-up' className='fs-2' />
        Export
      </button>
      {/* end::Export */}

      {/* begin::Add user */}
      <button type='button' className='btn btn-primary' onClick={openAddUserModal}>
        <KTIcon iconName='plus' className='fs-2' />
        Add Dataset
      </button>
      {/* end::Add user */}
      {/* <UserAddModal /> */}
      {/* {itemIdForUpdate !== undefined && <UserAddModal />} */}
    </div>
  )
}

export {UsersListToolbar}
