import React, { useState } from "react";
import { toAbsoluteUrl } from "../../../../../../_metronic/helpers";
import {
  IProfileDetails,
  profileDetailsInitValues as initialValues
} from "../SettingsModel";
import * as Yup from "yup";
import { useFormik } from "formik";
import { SignInMethod } from "./SignInMethod";
import axios from 'axios';

const ageGroup = [
  {
    age: "18-24",
    value: 0
  },
  {
    age: "25-29",
    value: 1
  },
  {
    age: "30-34",
    value: 2
  },
  {
    age: "35-39",
    value: 3
  },
  {
    age: "40-44",
    value: 4
  },
  {
    age: "45-49",
    value: 5
  },
  {
    age: "50-54",
    value: 6
  },
  {
    age: "55-59",
    value: 7
  },
  {
    age: "60-64",
    value: 8
  },
  {
    age: "65-69",
    value: 9
  },
  {
    age: "70-74",
    value: 10
  },
  {
    age: "75-79",
    value: 11
  },
  {
    age: "80 or older",
    value: 12
  }
];

const ethnicityGroup = [
  {
    ethnicity: "American Indian/Alaskan Native",
    value: 0
  },
  {
    ethnicity: "Asian",
    value: 1
  },
  {
    ethnicity: "Black",
    value: 2
  },
  {
    ethnicity: "Hispanic",
    value: 3
  },
  {
    ethnicity: "Other",
    value: 4
  },
  {
    ethnicity: "White",
    value: 5
  }
];

const profileDetailsSchema = Yup.object().shape({
  ageCategory: Yup.string().required("ageCategory is required"),
  biologicalSex: Yup.string().required("Biological Sex is required"),
  ethnicity: Yup.string().required("Ethnicity is required"),
  bmi: Yup.string().required("BMI is required"),
  heartDisease: Yup.string().required("Heart Disease is required"),
  smoker: Yup.string().required("Smoker is required"),
  stroke: Yup.string().required("Stroke is required"),
  lifestyle: Yup.string().required("Lifestyle is required"),
  mentalHealth: Yup.string().required("MentalHealth is required"),
  diffcultyWalking: Yup.string().required("Diffculty Walking is required"),
  diabetic: Yup.string().required("Diabetic is required"),
  asthma: Yup.string().required("Asthma is required"),
  kidneyDisease: Yup.string().required("Kidney Disease is required"),
  incontinence: Yup.string().required("Incontinence is required"),
  skinCancer: Yup.string().required("Skin Cancer is required")
});

const ProfileDetails: React.FC = () => {
  const [data, setData] = useState<IProfileDetails>(initialValues);
  const updateData = (fieldsToUpdate: Partial<IProfileDetails>): void => {
    const updatedData = Object.assign(data, fieldsToUpdate);
    setData(updatedData);
  };

  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState("");
  const formik = useFormik<IProfileDetails>({
    initialValues,
    validationSchema: profileDetailsSchema,
    onSubmit: async (values) => {
      setLoading(true);
      const updatedData = Object.assign(data, values);
      console.log(updatedData);
      const result = await axios.post('http://192.241.139.130:7000/heart-disease', updatedData);
      setResult(result.data);
      setLoading(false)
      // setTimeout(() => {
      //   // values.communications.email = data.communications.email
      //   // values.communications.phone = data.communications.phone
      //   // values.allowMarketing = data.allowMarketing
      //   const updatedData = Object.assign(data, values);
      //   setData(updatedData);
      //   setLoading(false);
      // }, 1000);
    }
  });

  return (
    <>
      <div className="card mb-5 mb-xl-10">
        <div
          className="card-header border-0 cursor-pointer"
          role="button"
          data-bs-toggle="collapse"
          data-bs-target="#kt_account_profile_details"
          aria-expanded="true"
          aria-controls="kt_account_profile_details"
        >
          <div className="card-title m-0">
            <h3 className="fw-bolder m-0">Prameters</h3>
          </div>
        </div>

        <div id="kt_account_profile_details" className="collapse show">
          <form onSubmit={formik.handleSubmit} noValidate className="form">
            <div className="card-body border-top p-9">
              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Age Group</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("ageCategory")}
                  >
                    <option value="">Select a Age Group...</option>
                    {ageGroup.map((ageRes, i) => (
                      <option value={ageRes.value} key={i}>
                        {ageRes.age}
                      </option>
                    ))}
                  </select>
                  {formik.touched.ageCategory && formik.errors.ageCategory && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.ageCategory}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Biological Sex</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("biologicalSex")}
                  >
                    <option value="">Select a Biological Sex...</option>
                    <option value="1">Male</option>
                    <option value="0">Female</option>
                  </select>
                  {formik.touched.biologicalSex && formik.errors.biologicalSex && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.biologicalSex}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Ethnicity</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("ethnicity")}
                  >
                    <option value="">Select a Ethnicity...</option>
                    {ethnicityGroup.map((ethnicityRes, i) => (
                      <option value={ethnicityRes.value} key={i}>
                        {ethnicityRes.ethnicity}
                      </option>
                    ))}
                  </select>
                  {formik.touched.ethnicity && formik.errors.ethnicity && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.ethnicity}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label required fw-bold fs-6">
                  BMI
                </label>

                <div className="col-lg-8 fv-row">
                  <input
                    type="number"
                    className="form-control form-control-lg form-control-solid"
                    placeholder="BMI"
                    {...formik.getFieldProps("bmi")}
                  />
                  {formik.touched.bmi && formik.errors.bmi && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">{formik.errors.bmi}</div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Heart Disease</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("heartDisease")}
                  >
                    <option value="">Select a Heart Disease...</option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                  {formik.touched.heartDisease && formik.errors.heartDisease && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.heartDisease}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Smoker</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("smoker")}
                  >
                    <option value="">Select if you smoke...</option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                  {formik.touched.smoker && formik.errors.smoker && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.smoker}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Stroke </span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("stroke")}
                  >
                    <option value="">
                      Has the subject been diagnosed with stroke...
                    </option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                  {formik.touched.stroke && formik.errors.stroke && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.stroke}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Lifestyle </span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("lifestyle")}
                  >
                    <option value="">
                      Select Lifestyle Over the last 30 days...
                    </option>
                    <option value="0">Sedentary</option>
                    <option value="1">Active</option>
                  </select>
                  {formik.touched.lifestyle && formik.errors.lifestyle && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.lifestyle}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Mental Health</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("mentalHealth")}
                  >
                    <option value="">Select a Mental Health...</option>
                    <option value="0">Poor</option>
                    <option value="1">Good</option>
                  </select>
                  {formik.touched.mentalHealth && formik.errors.mentalHealth && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.mentalHealth}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Difficulty Walking</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("diffcultyWalking")}
                  >
                    <option value="">
                      Does the subject have difficulty walking or climbing
                      stairs...
                    </option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                  {formik.touched.diffcultyWalking &&
                    formik.errors.diffcultyWalking && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.diffcultyWalking}
                        </div>
                      </div>
                    )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Diabetic</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("diabetic")}
                  >
                    <option value="">Select a if Diabetic...</option>
                    <option value="0">No</option>
                    <option value="1">No, borderline diabetes</option>
                    <option value="2">Yes</option>
                    <option value="3">Yes (during pregnancy)</option>
                  </select>
                  {formik.touched.diabetic && formik.errors.diabetic && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.diabetic}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Asthama </span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("asthma")}
                  >
                    <option value="">Select a if Asthama...</option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                  {formik.touched.asthma && formik.errors.asthma && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.asthma}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Kidney Disease</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("kidneyDisease")}
                  >
                    <option value="">Select a if Kidney Disease...</option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                  {formik.touched.kidneyDisease && formik.errors.kidneyDisease && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.kidneyDisease}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Incontinence</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("incontinence")}
                  >
                    <option value="">Select a Incontinence...</option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                  {formik.touched.incontinence && formik.errors.incontinence && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.incontinence}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-6">
                <label className="col-lg-4 col-form-label fw-bold fs-6">
                  <span className="required">Skin Cancer</span>
                </label>

                <div className="col-lg-8 fv-row">
                  <select
                    className="form-select form-select-solid form-select-lg fw-bold"
                    {...formik.getFieldProps("skinCancer")}
                  >
                    <option value="">Select a Skin Cancer...</option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                  {formik.touched.skinCancer && formik.errors.skinCancer && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.skinCancer}
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>

            <div className="card-footer d-flex justify-content-end py-6 px-9">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={loading}
              >
                {!loading && "Predict"}
                {loading && (
                  <span
                    className="indicator-progress"
                    style={{ display: "block" }}
                  >
                    Please wait...{" "}
                    <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                  </span>
                )}
              </button>
            </div>
          </form>
        </div>
      </div>
      <SignInMethod res={result} />
    </>
  );
};

export { ProfileDetails };
