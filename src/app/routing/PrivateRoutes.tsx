import {lazy, FC, Suspense} from 'react'
import {Route, Routes, Navigate} from 'react-router-dom'
import {MasterLayout} from '../../_metronic/layout/MasterLayout'
import TopBarProgress from 'react-topbar-progress-indicator'
import {DashboardWrapper} from '../pages/dashboard/DashboardWrapper'
import {MenuTestPage} from '../pages/MenuTestPage'
import {getCSSVariableValue} from '../../_metronic/assets/ts/_utils'
import {WithChildren} from '../../_metronic/helpers'
import BuilderPageWrapper from '../pages/layout-builder/BuilderPageWrapper'
import LoanApprovalPage from '../modules/loanApproval/LoanApprovalPage'
import HealthInsurancePage from '../modules/healthInsurance/HealthInsurancePage'
import TextClassifierPage from '../modules/textClassifier/TextClassifierPage'
import DataSciencePage from '../pages/layout-builder/DataSciencePage'
import BusinessPage from '../pages/layout-builder/BusinessPage'
import {Mixed} from '../modules/widgets/components/Mixed'
import ModelDraft from '../pages/ModelDraft/ModelDraft'
import DataInsight from '../pages/DataInsight/DataInsight'

const PrivateRoutes = () => {
  const ProfilePage = lazy(() => import('../modules/profile/ProfilePage'))
  const WizardsPage = lazy(() => import('../modules/wizards/WizardsPage'))
  const AccountPage = lazy(() => import('../modules/accounts/AccountPage'))
  const WidgetsPage = lazy(() => import('../modules/widgets/WidgetsPage'))
  const ChatPage = lazy(() => import('../modules/apps/chat/ChatPage'))
  const UsersPage = lazy(() => import('../modules/apps/user-management/UsersPage'))

  return (
    <Routes>
      <Route element={<MasterLayout />}>
        {/* Redirect to Dashboard after success login/registartion */}
        <Route path='auth/*' element={<Navigate to='/dashboard' />} />
        {/* Pages */}
        <Route path='dashboard' element={<DashboardWrapper />} />
        <Route path='builder' element={<BuilderPageWrapper />} />
        <Route path='dst' element={<DataSciencePage />} />
        <Route path='business' element={<BusinessPage />} />
        <Route path='menu-test' element={<MenuTestPage />} />
        <Route path='monitoring' element={<Mixed />} />
        <Route path='modelDraft' element={<ModelDraft />} />
        <Route path='dataInsight' element={<DataInsight />} />
        {/* Lazy Modules */}
        <Route
          path='crafted/pages/profile/*'
          element={
            <SuspensedView>
              <ProfilePage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/pages/wizards/*'
          element={
            <SuspensedView>
              <WizardsPage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/widgets/*'
          element={
            <SuspensedView>
              <WidgetsPage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/account/*'
          element={
            <SuspensedView>
              <AccountPage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/loanApproval/*'
          element={
            <SuspensedView>
              <LoanApprovalPage />
            </SuspensedView>
          }
        />
        <Route
          path='nlp/textClassifier/*'
          element={
            <SuspensedView>
              <TextClassifierPage />
            </SuspensedView>
          }
        />
        {/* {currentUser ? (
          <Route
            path="crafted/loanApproval/settings"
            element={
              <SuspensedView>
                <LoanApprovalPage />
              </SuspensedView>
            }
          />
        ) : (
          <>
            <Route path="auth/*" element={<AuthPage />} />
            <Route path="*" element={<Navigate to="/auth" />} />
          </>
        )} */}

        <Route
          path='crafted/healthInsurance/*'
          element={
            <SuspensedView>
              <HealthInsurancePage />
            </SuspensedView>
          }
        />
        <Route
          path='apps/chat/*'
          element={
            <SuspensedView>
              <ChatPage />
            </SuspensedView>
          }
        />
        <Route
          path='apps/user-management/*'
          element={
            <SuspensedView>
              <UsersPage />
            </SuspensedView>
          }
        />
        {/* Page Not Found */}
        <Route path='*' element={<Navigate to='/error/404' />} />
      </Route>
    </Routes>
  )
}

const SuspensedView: FC<WithChildren> = ({children}) => {
  const baseColor = getCSSVariableValue('--bs-primary')
  TopBarProgress.config({
    barColors: {
      '0': baseColor,
    },
    barThickness: 1,
    shadowBlur: 5,
  })
  return <Suspense fallback={<TopBarProgress />}>{children}</Suspense>
}

export {PrivateRoutes}
