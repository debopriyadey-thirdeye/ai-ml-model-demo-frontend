import React, { FC } from "react";
import { PageTitle } from "../../../_metronic/layout/core";
import { BuilderPage } from "./BuilderPage";
import { KTIcon } from "../../../_metronic/helpers";
import UsersPage from "../../modules/apps/user-management/UsersPage";

const data = [
  {
    modelName: 'Heart_Disease_Prediction',
    version: "1.3.4",
    ruuningOn: 'AWS',
    onlineUrl: '/crafted/account/settings',
  },
  {
    modelName: 'Loan_Approval_Prediction',
    version: "3.1.1",
    ruuningOn: 'Azure',
    onlineUrl: '/crafted/loanApproval/settings',
  },
  {
    modelName: 'Text_Classifier',
    version: "1.1.2",
    ruuningOn: 'GCP',
    onlineUrl: '/nlp/textClassifier/settings',
  },
]

const BusinessPage: FC = () => {
  return (
    <>
      {/* <PageTitle breadcrumbs={[]}>Model Management </PageTitle> */}
      {/* <UsersPage /> */}
      <div className='card'>
        <div className='card-header border-0 pt-6'>
          <div className='card-title'>
            <div className='d-flex align-items-center position-relative my-1'>
              <i className='ki-duotone ki-magnifier fs-1 position-absolute ms-6'>
                <span className='path1'></span>
                <span className='path2'></span>
              </i>
              <input
                type='text'
                data-kt-user-table-filter='search'
                className='form-control form-control-solid w-250px ps-14'
                placeholder='Search Model'
                value=''
              />
            </div>
          </div>
          <div className='card-toolbar'>
            <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
              <button
                type='button'
                className='btn btn-light-primary me-3'
                data-kt-menu-trigger='click'
                data-kt-menu-placement='bottom-end'
              >
                <i className='ki-duotone ki-filter fs-2'>
                  <span className='path1'></span>
                  <span className='path2'></span>
                </i>
                Filter
              </button>
              <div
                className='menu menu-sub menu-sub-dropdown w-300px w-md-325px'
                data-kt-menu='true'
              >
                <div className='px-7 py-5'>
                  <div className='fs-5 text-dark fw-bolder'>Filter Options</div>
                </div>
                <div className='separator border-gray-200'></div>
                <div className='px-7 py-5' data-kt-user-table-filter='form'>
                  <div className='mb-10'>
                    <label className='form-label fs-6 fw-bold'>Type:</label>
                    <select
                      className='form-select form-select-solid fw-bolder'
                      data-kt-select2='true'
                      data-placeholder='Select option'
                      data-allow-clear='true'
                      data-kt-user-table-filter='role'
                      data-hide-search='true'
                    >
                      <option value=''></option>
                      <option value='Administrator'>Administrator</option>
                      <option value='Analyst'>Analyst</option>
                      <option value='Developer'>Developer</option>
                      <option value='Support'>Support</option>
                      <option value='Trial'>Trial</option>
                    </select>
                  </div>
                  <div className='mb-10'>
                    <label className='form-label fs-6 fw-bold'>Last login:</label>
                    <select
                      className='form-select form-select-solid fw-bolder'
                      data-kt-select2='true'
                      data-placeholder='Select option'
                      data-allow-clear='true'
                      data-kt-user-table-filter='two-step'
                      data-hide-search='true'
                    >
                      <option value=''></option>
                      <option value='Yesterday'>Yesterday</option>
                      <option value='20 mins ago'>20 mins ago</option>
                      <option value='5 hours ago'>5 hours ago</option>
                      <option value='2 days ago'>2 days ago</option>
                    </select>
                  </div>
                  <div className='d-flex justify-content-end'>
                    <button
                      type='button'
                      className='btn btn-light btn-active-light-primary fw-bold me-2 px-6'
                      data-kt-menu-dismiss='true'
                      data-kt-user-table-filter='reset'
                    >
                      Reset
                    </button>
                    <button
                      type='button'
                      className='btn btn-primary fw-bold px-6'
                      data-kt-menu-dismiss='true'
                      data-kt-user-table-filter='filter'
                    >
                      Apply
                    </button>
                  </div>
                </div>
              </div>
              <button type='button' className='btn btn-light-primary me-3'>
                <i className='ki-duotone ki-exit-up fs-2'>
                  <span className='path1'></span>
                  <span className='path2'></span>
                </i>
                Export
              </button>
              <button type='button' className='btn btn-primary'>
                <i className='ki-duotone ki-plus fs-2'></i>Add Models
              </button>
            </div>
          </div>
        </div>
        <div className='card-body py-4'>
          <div className='table-responsive'>
            <table
              id='kt_table_users'
              className='table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer text-center'
              role='table'
            >
              <thead>
                <tr className='text-muted fw-bolder fs-7 text-uppercase gs-0'>
                  <th role='columnheader' className='w-10px pe-2'>
                    <div className='form-check form-check-sm form-check-custom form-check-solid me-3'>
                      <input
                        className='form-check-input'
                        type='checkbox'
                        data-kt-check='false'
                        data-kt-check-target='#kt_table_users .form-check-input'
                      />
                    </div>
                  </th>
                  <th role='columnheader' className='min-w-125px' style={{cursor: 'pointer'}}>
                    Hosted Model
                  </th>
                  <th
                    role='columnheader'
                    className='min-w-125px text-center'
                    style={{cursor: 'pointer'}}
                  >
                    Version
                  </th>
                  <th
                    role='columnheader'
                    className='min-w-125px text-center'
                    style={{cursor: 'pointer'}}
                  >
                    Running On
                  </th>
                  <th
                    role='columnheader'
                    className='min-w-125px text-center'
                    style={{cursor: 'pointer'}}
                  >
                    Online Prediction
                  </th>
                  <th
                    role='columnheader'
                    className='min-w-125px d-flex justify-content-center'
                    style={{cursor: 'pointer'}}
                  >
                    Batch Prediction
                  </th>
                  {/* <th
                    role="columnheader"
                    className="text-end min-w-100px"
                    style={{ cursor: "pointer" }}
                  >
                    Actions
                  </th> */}
                </tr>
              </thead>
              <tbody className='text-gray-600 fw-bold' role='rowgroup'>
                {data.map((item) => (
                  <tr role='row'>
                    <td role='cell' className='text-center'>
                      <div className='form-check form-check-custom form-check-solid'>
                        <input
                          className='form-check-input'
                          type='checkbox'
                          data-kt-check='false'
                          data-kt-check-target='#kt_table_users .form-check-input'
                        />
                      </div>
                    </td>
                    <td role='cell' className=''>
                      <div className='d-flex align-items-center justify-content-center'>
                        <div className='symbol symbol-circle symbol-50px overflow-hidden me-3'>
                          <a href='#'></a>
                        </div>
                        <div className='d-flex flex-column'>
                          <a href='#' className='text-gray-800 text-hover-primary mb-1'>
                            {item.modelName}
                          </a>
                        </div>
                      </div>
                    </td>
                    <td role='cell' className=''>
                      v{item.version}
                    </td>
                    <td role='cell' className=''>
                      <div className='badge badge-light fw-bolder'>
                        {item.ruuningOn}
                      </div>
                    </td>
                    <td role='cell' className='text-center'>
                      <div className='btn btn-sm fw-bold btn-primary'>
                        <a href={item.onlineUrl} style={{color: 'inherit'}}>
                          Try Out
                        </a>
                      </div>
                    </td>
                    <td role='cell' className='d-flex justify-content-center'>
                      <div className='btn btn-sm fw-bold btn-primary'>Try Out</div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  )
};

export default BusinessPage;
