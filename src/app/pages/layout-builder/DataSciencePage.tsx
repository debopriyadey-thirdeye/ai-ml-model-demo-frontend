/* eslint-disable jsx-a11y/anchor-is-valid */
import clsx from "clsx";
import React, { useState } from "react";
import { KTIcon, toAbsoluteUrl } from "../../../_metronic/helpers";
import {
  getLayoutFromLocalStorage,
  ILayout,
  LayoutSetup
} from "../../../_metronic/layout/core";
import { UsersTable } from "../../modules/apps/user-management/users-list/table/UsersTable";
import UsersPage from "../../modules/apps/user-management/UsersPage";
import BusinessPage from "./BusinessPage";
import ModelRegistry from "./ModelRegistry";

const DataSciencePage: React.FC = () => {
  const [dataSet, setDataSet] = useState("");
  const [tab, setTab] = useState("model_training");

  return (
    <>
      <div className='d-flex align-items-center gap-2 gap-lg-3 justify-content-end mb-4'>
        <a
          href='http://192.241.139.130:5000'
          target='_blank'
          className='btn btn-sm fw-bold btn-primary'
        >
          Model Assets Management
        </a>
      </div>
      {/* <div className='card mb-10'>
        <div className='card-body d-flex align-items-center py-8'>
          <div className='d-flex h-80px w-80px flex-shrink-0 flex-center position-relative'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              viewBox='0 0 24 24'
              className='text-primary h-75px w-75px h-lg-100px w-lg-100px position-absolute opacity-5'
            >
              <path
                fill='currentColor'
                d='M10.2,21.23,4.91,18.17a3.58,3.58,0,0,1-1.8-3.11V8.94a3.58,3.58,0,0,1,1.8-3.11L10.2,2.77a3.62,3.62,0,0,1,3.6,0l5.29,3.06a3.58,3.58,0,0,1,1.8,3.11v6.12a3.58,3.58,0,0,1-1.8,3.11L13.8,21.23A3.62,3.62,0,0,1,10.2,21.23Z'
              ></path>
            </svg>
            <KTIcon iconName='wrench' className='fs-2x fs-lg-3x text-primary position-absolute' />
          </div>

          <div className='ms-6'>
            <p className='list-unstyled text-gray-600 fw-bold fs-6 p-0 m-0'>
              The layout builder is to assist your set and configure your preferred project layout
              specifications and preview it in real-time.
            </p>
            <p className='list-unstyled text-gray-600 fw-bold fs-6 p-0 m-0'>
              Also, you can configurate the Layout in the code (
              <code>src/_metronic/layout/core/_LayoutConfig.ts</code> file). Don't forget clear your
              local storage when you are changing _LayoutConfig.
            </p>
          </div>
        </div>
      </div> */}

      <div className='card card-custom'>
        <div className='card-header card-header-stretch overflow-auto'>
          <ul
            className='nav nav-stretch nav-line-tabs
            fw-bold
            border-transparent
            flex-nowrap
          '
            role='tablist'
          >
            <li className='nav-item'>
              <a
                className={clsx(`nav-link cursor-pointer`, {
                  active: tab === 'model_training',
                })}
                onClick={() => setTab('model_training')}
                role='tab'
              >
                Model Training
              </a>
            </li>
            <li className='nav-item'>
              <a
                className={clsx(`nav-link cursor-pointer`, {
                  active: tab === 'model_registry',
                })}
                onClick={() => setTab('model_registry')}
                role='tab'
              >
                Model Registry
              </a>
            </li>
            <li className='nav-item'>
              <a
                className={clsx(`nav-link cursor-pointer`, {active: tab === 'deployed_endpoint'})}
                onClick={() => setTab('deployed_endpoint')}
                role='tab'
              >
                Deployed Endpoint
              </a>
            </li>
          </ul>
        </div>

        <form className='form'>
          <div className='card-body'>
            <div className='tab-content pt-3'>
              <div className={clsx('tab-pane', {active: tab === 'model_training'})}>
                <div className='row'>
                  <label className='col-lg-4 col-form-label fw-bold fs-6'>
                    <span className='required'>Dataset</span>
                  </label>

                  <div className='col-lg-8 fv-row'>
                    <select
                      className='form-select form-select-solid form-select-lg fw-bold'
                      value={dataSet}
                      onChange={(e) => {
                        setDataSet(e.target.value)
                      }}
                      // {...formik.getFieldProps("biologicalSex")}
                    >
                      <option value=''>Select a Dataset...</option>
                      <option value='Train_Text_Data'>Train_Text_Data</option>
                      <option value='Object_Detection'>Object_Detection</option>
                      <option value='Text_Classifier'>Text_Classifier</option>
                    </select>
                    {/* {formik.touched.biologicalSex && formik.errors.biologicalSex && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.biologicalSex}
                      </div>
                    </div>
                  )} */}
                  </div>
                </div>
                <div className='card mb-10'>
                  {dataSet !== '' && (
                    <iframe src='http://192.241.139.130:8080' height='600'></iframe>
                  )}
                </div>
              </div>
              <div className={clsx('tab-pane', {active: tab === 'model_registry'})}>
                <ModelRegistry/>
              </div>
              <div className={clsx('tab-pane', {active: tab === 'deployed_endpoint'})}>
                <BusinessPage/>
              </div>
            </div>
          </div>
        </form>
      </div>
    </>
  )
};

export default DataSciencePage ;
