import React, { FC } from "react";
import { PageTitle } from "../../../_metronic/layout/core";
import { BuilderPage } from "./BuilderPage";
import { KTIcon } from "../../../_metronic/helpers";

const BuilderPageWrapper: FC = () => {
  return (
    <>
      <PageTitle breadcrumbs={[]}>Model Management </PageTitle>
      
      <BuilderPage />
    </>
  );
};

export default BuilderPageWrapper;
