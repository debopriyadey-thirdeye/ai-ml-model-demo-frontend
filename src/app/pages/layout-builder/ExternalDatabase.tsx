/* eslint-disable jsx-a11y/anchor-is-valid */
import clsx from "clsx";
import React, { useState } from "react";
import { PageTitle } from "../../../_metronic/layout/core";
import { KTIcon } from "../../../_metronic/helpers";

const ExternalDatabase: React.FC = () => {
  const [dataSet, setDataSet] = useState("");
  const [tab, setTab] = useState("model_training");
  const [stepCompleted, setStepCompleted] = useState<Array<number>>([]);

  const nextStep = (step: number) => {
    setStepCompleted([...stepCompleted, step]);
  };

  return (
    <>
      <PageTitle breadcrumbs={[]}>Add New External Database </PageTitle>
      <div className="card card-custom">
        {/*begin::Stepper*/}
        <div
          className="stepper stepper-pills"
          id="kt_stepper_example_clickable"
        >
          {/*begin::Nav*/}
          <div className="card-header card-header-stretch overflow-auto">
            <div className="stepper-nav flex-center flex-wrap mb-10">
              {/*begin::Step 1*/}
              <div
                className={`stepper-item mx-8 my-4 ${
                  stepCompleted[stepCompleted.length - 1] == undefined
                    ? "current"
                    : "completed"
                }`}
                data-kt-stepper-element="nav"
                data-kt-stepper-action="step"
              >
                {/*begin::Wrapper*/}
                <div className="stepper-wrapper d-flex align-items-center">
                  {/*begin::Icon*/}
                  <div className="stepper-icon w-40px h-40px">
                    <i className="stepper-check fas fa-check"></i>
                    <span className="stepper-number">1</span>
                  </div>
                  {/*end::Icon*/}

                  {/*begin::Label*/}
                  <div className="stepper-label">
                    <h3 className="stepper-title">Step 1</h3>

                    <div className="stepper-desc">Description</div>
                  </div>
                  {/*end::Label*/}
                </div>
                {/*end::Wrapper*/}

                {/*begin::Line*/}
                <div className="stepper-line h-40px"></div>
                {/*end::Line*/}
              </div>
              {/*end::Step 1*/}

              {/*begin::Step 2*/}
              <div
                className={`stepper-item mx-8 my-4 ${
                  stepCompleted[stepCompleted.length - 1] == undefined
                    ? ""
                    : stepCompleted[stepCompleted.length - 1] == 1
                    ? "current"
                    : "completed"
                }`}
                data-kt-stepper-element="nav"
                data-kt-stepper-action="step"
              >
                {/*begin::Wrapper*/}
                <div className="stepper-wrapper d-flex align-items-center">
                  {/*begin::Icon*/}
                  <div className="stepper-icon w-40px h-40px">
                    <i className="stepper-check fas fa-check"></i>
                    <span className="stepper-number">2</span>
                  </div>
                  {/*begin::Icon*/}

                  {/*begin::Label*/}
                  <div className="stepper-label">
                    <h3 className="stepper-title">Step 2</h3>

                    <div className="stepper-desc">Description</div>
                  </div>
                  {/*end::Label*/}
                </div>
                {/*end::Wrapper*/}

                {/*begin::Line*/}
                <div className="stepper-line h-40px"></div>
                {/*end::Line*/}
              </div>
              {/*end::Step 2*/}

              {/*begin::Step 3*/}
              <div
                className={`stepper-item mx-8 my-4 ${
                  stepCompleted[stepCompleted.length - 1] == 1
                    ? ""
                    : stepCompleted[stepCompleted.length - 1] == 2
                    ? "current"
                    : ""
                }`}
                data-kt-stepper-element="nav"
                data-kt-stepper-action="step"
              >
                {/*begin::Wrapper*/}
                <div className="stepper-wrapper d-flex align-items-center">
                  {/*begin::Icon*/}
                  <div className="stepper-icon w-40px h-40px">
                    <i className="stepper-check fas fa-check"></i>
                    <span className="stepper-number">3</span>
                  </div>
                  {/*begin::Icon*/}

                  {/*begin::Label*/}
                  <div className="stepper-label">
                    <h3 className="stepper-title">Step 3</h3>

                    <div className="stepper-desc">Description</div>
                  </div>
                  {/*end::Label*/}
                </div>
                {/*end::Wrapper*/}

                {/*begin::Line*/}
                <div className="stepper-line h-40px"></div>
                {/*end::Line*/}
              </div>
              {/*end::Step 3*/}
            </div>
            {/*end::Nav*/}
          </div>

          {/*begin::Form*/}

          <div
            className="form w-lg-700px mx-auto"
            // novalidate="novalidate"
            id="kt_stepper_example_basic_form"
          >
            <div className="my-5">
              {/* Step 1 */}
              <div
                className={`flex-column ${
                  stepCompleted[stepCompleted.length - 1] == undefined
                    ? "current"
                    : "completed"
                }`}
              >
                <div>
                  <div className="d-flex flex-wrap flex-stack">
                    <div className="d-flex flex-column flex-grow-1 pe-8">
                      <div className="d-flex flex-wrap">
                        <button
                          className="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3"
                          onClick={(e) => nextStep(1)}
                        >
                          <div className="d-flex align-items-center justify-content-center">
                            <img
                              src="https://www.svgrepo.com/show/331488/mongodb.svg"
                              height={40}
                            />
                          </div>

                          <div className="fw-bold fs-6 text-gray-400">
                            MongoDB Atlas
                          </div>
                        </button>

                        <button className="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                          <div className="d-flex align-items-center">
                            <div className="d-flex align-items-center justify-content-center">
                              <img
                                src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Amazon-S3-Logo.svg/1200px-Amazon-S3-Logo.svg.png"
                                height={40}
                              />
                            </div>
                          </div>

                          <div className="fw-bold fs-6 text-gray-400">
                            Amazon S3
                          </div>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* Step 2 */}
              <div
                className={`flex-column ${
                  stepCompleted[stepCompleted.length - 1] == undefined
                    ? "pending"
                    : stepCompleted[stepCompleted.length - 1] == 1
                    ? "current"
                    : "completed"
                }`}
                data-kt-stepper-element="content"
              >
                <div className="fv-row mb-10">
                  <label className="form-label">Connection URL</label>
                  <input
                    type="text"
                    className="form-control form-control-solid"
                    name="input1"
                    placeholder=""
                    value=""
                  />
                </div>

                <div className="fv-row mb-10">
                  <label className="form-label">Password</label>
                  <input
                    type="text"
                    className="form-control form-control-solid"
                    name="input1"
                    placeholder=""
                    value=""
                  />
                </div>
                <button
                  type="button"
                  className="btn btn-primary"
                  data-kt-stepper-action="next"
                  onClick={(e) => nextStep(2)}
                >
                  Continue
                </button>
              </div>
              {/* Step 3 */}
              <div
                className={`flex-column ${
                  stepCompleted[stepCompleted.length - 1] == 1
                    ? "pending"
                    : stepCompleted[stepCompleted.length - 1] == 2
                    ? "current"
                    : "pending"
                }`}
              >
                <div className="d-flex flex-stack py-4">
                  <div className="d-flex align-items-center">
                    <div className="symbol symbol-35px me-4">
                      <span className="symbol-label bg-light-primary">
                        {" "}
                        <i className="ki-duotone ki-technology-2 fs-2 text-primary">
                          <span className="path1"></span>
                          <span className="path2"></span>
                        </i>
                      </span>
                    </div>
                    <div className="mb-0 me-2">
                      <a
                        href="#"
                        className="fs-6 text-gray-800 text-hover-primary fw-bolder"
                      >
                        Test your connection before fetching data
                      </a>
                      <div className="text-gray-400 fs-7">
                        Phase 1 development
                      </div>
                    </div>
                  </div>
                  <button
                    type="button"
                    className="btn btn-primary me-3"
                    data-kt-stepper-action="next"
                  >
                    Test Connection
                  </button>
                </div>
              </div>
            </div>
          </div>
          {/*end::Form*/}
        </div>
        {/*end::Stepper*/}
      </div>
    </>
  );
};

export default ExternalDatabase;
