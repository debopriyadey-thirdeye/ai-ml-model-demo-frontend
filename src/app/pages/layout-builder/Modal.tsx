import React from 'react'

const Modal = () => {
  return (
    <div
      className='modal fade'
      id='exampleModal'
      aria-labelledby='exampleModalLabel'
      aria-hidden='true'
    >
      <div className='modal-dialog'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h1 className='modal-title fs-5' id='exampleModalLabel'>
              Choose where you want to Deploy
            </h1>
            <button
              type='button'
              className='btn-close'
              data-bs-dismiss='modal'
              aria-label='Close'
            ></button>
          </div>
          <div className='modal-body d-flex flex-column'>
            <a
              href='http://192.241.139.130:8080'
              className='menu-item me-lg-1'
              style={{color: '#5E6278'}}
              target='_blank'
            >
              Azure
            </a>
            <a
              href='http://192.241.139.130:8080'
              className='menu-item me-lg-1'
              style={{color: '#5E6278'}}
              target='_blank'
            >
              GCP
            </a>
            <a
              href='http://192.241.139.130:8080'
              className='menu-item me-lg-1'
              style={{color: '#5E6278'}}
              target='_blank'
            >
              AWS
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Modal
