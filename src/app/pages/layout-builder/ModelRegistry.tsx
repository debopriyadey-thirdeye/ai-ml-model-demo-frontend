import { useState } from "react";
import { MenuInner } from "../../../_metronic/layout/components/header/header-menus";
import { MenuInnerWithSub } from "../../../_metronic/layout/components/header/header-menus/MenuInnerWithSub";
import { MenuItem } from "../../../_metronic/layout/components/header/header-menus/MenuItem";
import { ReactComponent as OptionSvg } from "../../threeDots.svg";

const data = [
  {
    modelName: "Heart_Disease_Prediction",
    version: "1.3.4",
    status: "Raw",
    type: "ML",
    lastUpdated: "15 April 2023, 9:23 pm"
  },
  {
    modelName: "Loan_Approval_Prediction",
    version: "3.1.1",
    status: "Staging",
    type: "ML",
    lastUpdated: "13 April 2023, 9:23 pm"
  },
  {
    modelName: "Text_Classifier",
    version: "1.1.2",
    status: "Prod",
    type: "NLP",
    lastUpdated: "10 April 2023, 9:23 pm"
  }
];

const ModelRegistry = () => {
  const [show, setShow] = useState(false);

  return (
    <>
      <div className="card-body py-4">
        <div className="table-responsive">
          <table
            id="kt_table_users"
            className="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer text-center"
            role="table"
          >
            <thead>
              <tr className="text-muted fw-bolder fs-7 text-uppercase gs-0">
                <th
                  role="columnheader"
                  className="min-w-125px"
                  style={{ cursor: "pointer" }}
                >
                  Name
                </th>
                <th
                  role="columnheader"
                  className="min-w-125px text-center"
                  style={{ cursor: "pointer" }}
                >
                  Version
                </th>
                <th
                  role="columnheader"
                  className="min-w-125px text-center"
                  style={{ cursor: "pointer" }}
                >
                  Status
                </th>
                <th
                  role="columnheader"
                  className="min-w-125px text-center"
                  style={{ cursor: "pointer" }}
                >
                  Type
                </th>
                <th
                  role="columnheader"
                  className="min-w-125px d-flex justify-content-center"
                  style={{ cursor: "pointer" }}
                >
                  Last Updated
                </th>
                <th
                  role="columnheader"
                  className="text-end min-w-100px text-center"
                  style={{ cursor: "pointer" }}
                >
                  
                </th>
              </tr>
            </thead>
            <tbody className="text-gray-600 fw-bold" role="rowgroup">
              {data.map((item) => (
                <tr role="row">
                  <td role="cell" className="">
                    <div className="d-flex align-items-center justify-content-center">
                      <div className="symbol symbol-circle symbol-50px overflow-hidden me-3">
                        <a href="#"></a>
                      </div>
                      <div className="d-flex flex-column">
                        <a
                          href="#"
                          className="text-gray-800 text-hover-primary mb-1"
                        >
                          {item.modelName}
                        </a>
                      </div>
                    </div>
                  </td>
                  <td role="cell" className="">
                    v{item.version}
                  </td>
                  <td role="cell" className="">
                    <div className="">
                      {item.status}
                    </div>
                  </td>
                  <td role="cell" className="text-center">
                    {item.type}
                  </td>
                  <td role="cell" className="d-flex justify-content-center">
                    {item.lastUpdated}
                  </td>
                  {/* <td role='cell' className='cursor-pointer'>
                    <div className='btn-group'>
                      <button
                        type='button'
                        className='dropdown-toggle bg-transparent border-white'
                        data-bs-toggle='dropdown'
                        aria-expanded='false'
                      >
                        <OptionSvg />
                      </button>
                      <ul className='dropdown-menu dropdown-menu-end'>
                        <li>
                          <button className='dropdown-item' type='button'>
                            Edit Version
                          </button>
                        </li>
                        <li>
                          <button className='dropdown-item' type='button'>
                            Change Status
                          </button>
                        </li>
                        <li>
                            Deploy to Endpoint
                        </li>
                      </ul>
                    </div>
                  </td> */}
                  <td role="cell" className="min-w-100px">
                    <div
                      className="menu
                          menu-rounded
                          menu-column
                          menu-lg-row
                          my-1
                          my-lg-0
                          align-items-stretch
                          fw-semibold
                          px-1 px-lg-0
                          d-flex
                          justify-content-center
                          "
                      id="kt_app_header_menu"
                      data-kt-menu="true"
                      style={{backgroundColor: "#F9F9F9", borderRadius: "5px"}}
                    >
                      <MenuInnerWithSub
                        title="Action"
                        to="/apps"
                        hasArrow={true}
                        menuPlacement="bottom-start"
                        menuTrigger="click"
                      >
                        <MenuItem
                          icon="abstract-28"
                          to="/apps/user-management/users"
                          title="Edit Version"
                        />
                        <MenuItem
                          icon="abstract-28"
                          to="/apps/user-management/users"
                          title="Change Status"
                        />
                        <MenuInnerWithSub
                          title="Deploy to Endpoint"
                          to="/dst"
                          icon="message-text-2"
                          hasArrow={true}
                          menuPlacement="right-start"
                          menuTrigger={`{default:'click', lg: 'hover'}`}
                        >
                          <MenuItem
                            to="/dst"
                            title="Azure"
                            hasBullet={true}
                            click={() => {setShow(true)}}
                          />
                          <MenuItem
                            to="/dst"
                            title="GCP"
                            hasBullet={true}
                            click={() => {setShow(true)}}
                          />
                          <MenuItem
                            to="/dst"
                            title="AWS"
                            hasBullet={true}
                            click={() => {setShow(true)}}
                          />
                        </MenuInnerWithSub>
                      </MenuInnerWithSub>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        {show && (
          <iframe src="http://192.241.139.130:8080" height="600" width="100%"></iframe>
        )}
      </div>
    </>
  );
};

export default ModelRegistry;
