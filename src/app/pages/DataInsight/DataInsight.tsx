import React, {useState} from 'react'
import UploadImagesFormModal from './UploadImagesForm/UploadImagesFormModal'
import {useMutation, useQuery} from 'react-query'
import Api from '../../../utils/Api/api'

export interface ResponseType {
  image: string
  occlusion_percentages: number
  scales: number
  rotations: number
  brightness_levels: number
  calculate_contrast_levels: number
  calculate_average_color_levels: number
  calculate_aspect_ratio_levels: number
  calculate_contour_count_levels: number
  calculate_sharpness_levels: number
}

export default function DataInsight() {
  const [isOpen, setIsOpen] = React.useState(false)
  const [dataTypes, setDataTypes] = useState<string>('')
  const {mutate: uploadImages} = useMutation({
    mutationFn: (data) => Api.uploadImages(data),
    onSuccess: () => {
      setReadsForTrainData(true)
      closeModal()
    },
  })
  const {refetch, isLoading, isFetching, data} = useQuery({
    queryFn: Api.analyzeData,
    queryKey: 'analyzeData',
    enabled: false,
  })
  const trainDataClickHandler = () => {
    refetch()
  }

  const [readyForTrainData, setReadsForTrainData] = useState(false)
  const openModal = () => {
    setIsOpen(true)
  }
  const closeModal = () => {
    setIsOpen(false)
  }
  const trainingDataClickHandler = () => {
    openModal()
    setDataTypes('Training Data')
  }
  const failedProductionDataClickHandler = () => {
    openModal()
    setDataTypes('Failed Production Data')
  }

  return (
    <div className='container'>
      <div className='flex-column gap-3'>
        <div className='text-center mb-5'>
          <h1 className='h1'> Data Insight</h1>
        </div>
        <div className='d-flex flex-row justify-content-between'>
          <button className='btn btn-primary' onClick={trainingDataClickHandler}>
            Connect to Training Data
          </button>
          <button className='btn btn-primary' onClick={failedProductionDataClickHandler}>
            Connect to Failed Production Data
          </button>
        </div>
        <div className='d-flex flex-row justify-content-center my-3'>
          <button
            className='btn btn-primary'
            onClick={trainDataClickHandler}
            disabled={!readyForTrainData}
          >
            Train Data
          </button>
        </div>
        <div className='d-flex justify-content-center flex-column my-5'>
          {isLoading && isFetching ? (
            <div>Loading...</div>
          ) : (
            data && (
              <>
                <h4 style={{textAlign: dataTypes === 'Failed Production Data' ? 'right' : 'left'}}>
                  {dataTypes}
                </h4>
                <div className='table-responsive'>
                  <table className='table  table-light table-striped  table-bordered mt-5 '>
                    <thead>
                      <tr>
                        <th colSpan={10} className='text-center'>
                          Average Change In Data
                        </th>
                      </tr>
                    </thead>
                    <thead className='table-primary p-3'>
                      <tr>
                        <th scope='col'>Image</th>
                        <th scope='col'>Occlusion (%)</th>
                        <th scope='col'>Scale</th>
                        <th scope='col'>Rotation (degrees)</th>
                        <th scope='col'>Brightness</th>
                        <th scope='col'>Contrast Level</th>
                        <th scope='col'>Average Color Level</th>
                        <th scope='col'>Aspect Ratio Levels</th>
                        <th scope='col'>Contour Count Levels</th>
                        <th scope='col'>Sharpness Levels</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.data.map((item: ResponseType) => {
                        return (
                          <tr key={item.image}>
                            <td>{item.image}</td>
                            <td>{item.occlusion_percentages}</td>
                            <td>{item.scales}</td>
                            <td>{item.rotations}</td>
                            <td>{item.brightness_levels}</td>
                            <td>{item.calculate_contrast_levels}</td>
                            <td>{item.calculate_average_color_levels}</td>
                            <td>{item.calculate_aspect_ratio_levels}</td>
                            <td>{item.calculate_contour_count_levels}</td>
                            <td>{item.calculate_sharpness_levels}</td>
                          </tr>
                        )
                      })}
                    </tbody>
                  </table>
                </div>
                <h2 className='mt-3'>{data?.message}</h2>
              </>
            )
          )}
        </div>
      </div>
      <UploadImagesFormModal
        title='Upload Image'
        isOpen={isOpen}
        openModal={openModal}
        closeModal={closeModal}
        uploadImages={uploadImages}
      />
    </div>
  )
}
