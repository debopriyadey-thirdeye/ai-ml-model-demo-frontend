import React, {useEffect, useState} from 'react'
import {Modal} from 'react-bootstrap'
import {useDropzone} from 'react-dropzone'

interface UploadImagesFormModalProps {
  title: string
  isOpen: boolean
  openModal: () => void
  closeModal: () => void
  uploadImages: Function
}

export default function UploadImagesFormModal({
  title,
  isOpen,
  closeModal,
  uploadImages,
}: UploadImagesFormModalProps) {
  const [minFilesError, setMinFilesError] = useState(false)
  const [files, setFiles] = useState<File[]>([])

  useEffect(() => {
    setFiles([])
  }, [])

  const handleDrop = (acceptedFiles: File[]) => {
    if (acceptedFiles.length >= 10 && acceptedFiles.length <= 100) {
      setMinFilesError(false)
      setFiles(acceptedFiles)
    } else {
      setMinFilesError(true)
    }
  }
  const {fileRejections, getRootProps, getInputProps} = useDropzone({
    accept: {'image/*': ['.jpeg', '.png']},
    maxFiles: 100,
    onDrop: handleDrop,
  })

  const acceptedFileItems = files.map((file: any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ))

  const fileRejectionItems = fileRejections.map(({file, errors}: any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
      <ul>
        {errors.map((e: any) => (
          <li key={e.code} className='error'>
            {e.message}
          </li>
        ))}
      </ul>
    </li>
  ))
  const uploadHandler = () => {
    console.log(files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('images', file)
    })
    console.log(formData)
    uploadImages(formData)
  }

  return (
    <div>
      <Modal show={isOpen} centered>
        <Modal.Header>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <section className='container'>
            <div {...getRootProps({className: 'dropzone'})}>
              <input {...getInputProps()} />
              <p className='fs-5 fw-bold text-gray-900 mb-1'>
                Drag 'n' drop some files here, or click to select files
              </p>
              <span className='fs-7 fw-semibold text-gray-400'>Upload up to 100 images</span>
              <em className='fs-7 fw-semibold text-gray-400'>
                (Only *.jpeg and *.png images will be accepted)
              </em>
            </div>
            <aside>
              {minFilesError && (
                <p className='error'>Please upload at least 10 files and Max 100 files</p>
              )}
              {files?.length > 0 ? (
                <h4 className='fs-5 fw-bold text-gray-900 mb-1 mt-2'>Accepted files</h4>
              ) : (
                <></>
              )}
              <ul>{acceptedFileItems}</ul>
              {fileRejectionItems.length > 0 ? (
                <h4 className='fs-5 fw-bold text-gray-900 mb-1 mt-2'>Rejected files</h4>
              ) : (
                <></>
              )}
              <ul>{fileRejectionItems}</ul>
            </aside>
          </section>
        </Modal.Body>
        <Modal.Footer>
          <button className='btn btn-primary' onClick={closeModal}>
            Cancel
          </button>
          <button className='btn btn-primary' disabled={minFilesError} onClick={uploadHandler}>
            Upload
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}
