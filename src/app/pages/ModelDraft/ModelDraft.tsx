import React from 'react'
import {useMutation} from 'react-query'
import * as yup from 'yup'
import {useForm} from 'react-hook-form'
import {yupResolver} from '@hookform/resolvers/yup'
import Api from '../../../utils/Api/api'
import {BASE_URL, SERVER_MEDIA_URL} from '../../../utils/config/config'
import './ModelDraft.css'

type Inputs = {
  tropic: string
  file: FileList
}

const schema = yup.object().shape({
  tropic: yup.string().required('Model is required'),
  file: yup.mixed().required('File is required'),
})

export default function ModelDraft() {
  const {
    register,
    handleSubmit,
    formState: {errors},
    getValues,
    reset,
  } = useForm<Inputs>({resolver: yupResolver(schema) as any})
  const {mutate, data, isLoading} = useMutation({
    mutationFn: (data: any) => Api.getModelMonitoringData(data),
    mutationKey: ['getModelMontoringResult'],
    onSuccess: (res) => {
      console.log(res)
    },
    onError: (res) => {
      console.log(res)
    },
  })
  const {mutate: uploadFile, isLoading: isLoading2} = useMutation({
    mutationFn: (data: any) => Api.uploadFile(data),
    mutationKey: ['uploadFile'],
    onSuccess: (res) => {
      console.log(res)
      mutate({
        url: `${BASE_URL}${res.file}`,
        label: getValues().tropic,
      })
      reset()
    },
    onError: (res) => {
      console.log(res)
    },
  })
  const onSubmitHandler = handleSubmit((data: Inputs) => {
    const f = new FormData()
    f.append('file', data.file[0])
    uploadFile(f)
  })

  return (
    <div className='myClass'>
      <div className='sidebar'>
        <h2>Get Result</h2>
        <form onSubmit={onSubmitHandler}>
          <div className='form-group'>
            <label htmlFor='dropdown'>Select a model:</label>
            <select className='form-control' id='dropdown' {...register('tropic')}>
              <option value=''>Select a model</option>
              <option value='all'>All models</option>
              <option value='cross_arm'>Cross Arm</option>
              <option value='pole'>Poles detection</option>
              <option value='tag'>Tag Description</option>
            </select>
            {errors.tropic && <span className='error'>{errors.tropic.message}</span>}
          </div>
          <div className='form-group custom-file-container mt-4'>
            {/* <label htmlFor='file' className='custom-file-label'>
              Choose a file:
            </label> */}
            <div className='custom-file'>
              <input
                {...register('file')}
                type='file'
                className='custom-file-input'
                name='file'
                required
                id='file'
                accept='.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'
              />
              {errors.file && <span className='error'>{errors.file.message}</span>}
            </div>
          </div>
          <button type='submit' className='btn btn-primary'>
            Submit
          </button>
        </form>
      </div>
      <div className='container mt-4'>
        {isLoading || isLoading2 ? (
          <div className='loader'>Loading...</div>
        ) : (
          <div className='row'>
            {data?.data?.slice(0, 3).map((ele: any) => (
              <div key={ele.label} className='col-md-4'>
                <div className='image-container'>
                  <img
                    src={`${SERVER_MEDIA_URL}${ele.path}`}
                    alt={ele.label}
                    className='img-fluid'
                  />
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  )
}
