import axios from "axios";
import { BASE_URL } from "../config/config";

const getModelMonitoringData=async(data:any)=>{
     const res=await axios.post(`${BASE_URL}model_monitoring/post-data/`,data,{
        headers:{
            
        }
     })
     return res.data
}


const uploadFile=async(data:any)=>{
    const res=await axios.post(`${BASE_URL}upload_file/upload-csv/`,data,{
       headers:{
           
       }
    })
    return res.data
}

const uploadImages=async(data:any)=>{
    const res=await axios.post(`${BASE_URL}modelInsight/uploadImages/`,data,{
    })
    return res.data
}
const analyzeData=async()=>{
    const res=await axios.get(`${BASE_URL}modelInsight/analyzeData/`,{
    })
    return res.data
}



const Api={
    getModelMonitoringData,
    uploadFile,
    uploadImages,
    analyzeData
}

export default Api
