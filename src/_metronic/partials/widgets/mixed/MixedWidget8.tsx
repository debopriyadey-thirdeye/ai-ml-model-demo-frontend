/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useRef, useState } from "react";
import ApexCharts, { ApexOptions } from "apexcharts";
import { KTIcon, toAbsoluteUrl } from "../../../helpers";
import { getCSSVariableValue } from "../../../assets/ts/_utils";
import { Dropdown1 } from "../../content/dropdown/Dropdown1";
import { useThemeMode } from "../../layout/theme-mode/ThemeModeProvider";
import graph1 from "../../../assets/graph/graph1.png";
import graph2 from "../../../assets/graph/graph2.png";
import graph3 from "../../../assets/graph/graph3.png";
import graph4 from "../../../assets/graph/graph4.png";

type Props = {
  className: string;
  chartColor: string;
  chartHeight: string;
};

const MixedWidget8: React.FC<Props> = ({
  className,
  chartColor,
  chartHeight
}) => {
  const chartRef = useRef<HTMLDivElement | null>(null);
  const { mode } = useThemeMode();
  const refreshChart = () => {
    if (!chartRef.current) {
      return;
    }

    const chart1 = new ApexCharts(
      chartRef.current,
      chart1Options(chartColor, chartHeight)
    );
    if (chart1) {
      chart1.render();
    }

    return chart1;
  };

  const [model, setModel] = useState("")

  useEffect(() => {
    const chart1 = refreshChart();

    return () => {
      if (chart1) {
        chart1.destroy();
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartRef, mode]);

  useEffect(() => {
    //api call
  }, [model])
  

  return (
    <div className={`card ${className}`}>
      {/* begin::Beader */}
      <div className="card-header border-0 py-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">
            Post-deployment model performance
          </span>

          <span className="text-muted fw-semibold fs-7">Model Drift</span>
        </h3>

        <div className="card-toolbar">
          {/* begin::Menu */}
          <button
            type="button"
            className="btn btn-sm btn-icon btn-color-primary btn-active-light-primary"
            data-kt-menu-trigger="click"
            data-kt-menu-placement="bottom-end"
            data-kt-menu-flip="top-end"
          >
            <KTIcon iconName="category" className="fs-2" />
          </button>
          <Dropdown1 />
          {/* end::Menu */}
        </div>
      </div>
      {/* end::Header */}

      {/* begin::Body */}
      <div className="card-body d-flex flex-column">
        <div className="row">
          <label className="col-lg-4 col-form-label fw-bold fs-6">
            <span className="required">Models</span>
          </label>

          <div className="col-lg-8 fv-row">
            <select
              className="form-select form-select-solid form-select-lg fw-bold"
              value={model}
              onChange={(e) => {
                setModel(e.target.value);
              }}
              // {...formik.getFieldProps("biologicalSex")}
            >
              <option value="">Select a Model...</option>
              <option value="Heart_Disease_Detection">Heart Disease Detection</option>
              <option value="Text_Classifier">Text Classifier</option>
              <option value="Pole_Crossarm_Detection">Pole Crossarm Detection</option>
            </select>
            {/* {formik.touched.biologicalSex && formik.errors.biologicalSex && (
                    <div className="fv-plugins-message-container">
                      <div className="fv-help-block">
                        {formik.errors.biologicalSex}
                      </div>
                    </div>
                  )} */}
          </div>
        </div>
        {model && <img src={graph1} alt="graph" className="m-4" />}
      </div>
      {/* <div className="card-body d-flex flex-column">
        <h3 className="card-title align-items-center flex-column">
          <span className="card-label fw-bold fs-3 mb-1">
            Multivariate drift (PCA reconstruction error)
          </span>
        </h3>
        <div className="mixed-widget-5-chart card-rounded-top d-flex justify-content-center">
          <img src={graph1} alt="graph" />
        </div>
        <br />
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">Realized performance</span>
        </h3>
        <div className="mixed-widget-5-chart card-rounded-top d-flex justify-content-center">
          <img src={graph2} alt="graph" />
        </div>
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">Univariate drift metrics</span>
        </h3>
        <div className="mixed-widget-5-chart card-rounded-top d-flex justify-content-center">
          <img src={graph3} alt="graph" />
        </div>
        <div className="mixed-widget-5-chart card-rounded-top d-flex justify-content-center">
          <img src={graph4} alt="graph" />
        </div>
      </div> */}
      {/* end::Body */}
    </div>
  );
};

const chart1Options = (
  chartColor: string,
  chartHeight: string
): ApexOptions => {
  const labelColor = getCSSVariableValue("--bs-gray-800");
  const strokeColor = getCSSVariableValue("--bs-gray-300");
  const baseColor = getCSSVariableValue("--bs-" + chartColor) as string;
  const lightColor = getCSSVariableValue("--bs-" + chartColor + "-light");

  return {
    series: [
      {
        name: "Net Profit",
        data: [30, 30, 60, 25, 25, 40]
      }
    ],
    chart: {
      fontFamily: "inherit",
      type: "area",
      height: chartHeight,
      toolbar: {
        show: false
      },
      zoom: {
        enabled: false
      },
      sparkline: {
        enabled: true
      }
    },
    plotOptions: {},
    legend: {
      show: false
    },
    dataLabels: {
      enabled: false
    },
    fill: {
      type: "solid",
      opacity: 1
    },
    fill1: {
      type: "gradient",
      opacity: 1,
      gradient: {
        type: "vertical",
        shadeIntensity: 0.5,
        gradientToColors: undefined,
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 0.375,
        stops: [25, 50, 100],
        colorStops: []
      }
    },
    stroke: {
      curve: "smooth",
      show: true,
      width: 3,
      colors: [baseColor]
    },
    xaxis: {
      categories: ["Feb", "Mar", "Apr", "May", "Jun", "Jul"],
      axisBorder: {
        show: false
      },
      axisTicks: {
        show: false
      },
      labels: {
        show: false,
        style: {
          colors: labelColor,
          fontSize: "12px"
        }
      },
      crosshairs: {
        show: false,
        position: "front",
        stroke: {
          color: strokeColor,
          width: 1,
          dashArray: 3
        }
      },
      tooltip: {
        enabled: true,
        formatter: undefined,
        offsetY: 0,
        style: {
          fontSize: "12px"
        }
      }
    },
    yaxis: {
      min: 0,
      max: 65,
      labels: {
        show: false,
        style: {
          colors: labelColor,
          fontSize: "12px"
        }
      }
    },
    states: {
      normal: {
        filter: {
          type: "none",
          value: 0
        }
      },
      hover: {
        filter: {
          type: "none",
          value: 0
        }
      },
      active: {
        allowMultipleDataPointsSelection: false,
        filter: {
          type: "none",
          value: 0
        }
      }
    },
    tooltip: {
      style: {
        fontSize: "12px"
      },
      y: {
        formatter: function (val) {
          return "$" + val + " thousands";
        }
      }
    },
    colors: [lightColor],
    markers: {
      colors: [lightColor],
      // @ts-ignore
      strokeColor: [baseColor],
      strokeWidth: 3
    }
  };
};

export { MixedWidget8 };
