import {FC, useState} from 'react'
import {useLocation} from 'react-router'
import {Link} from 'react-router-dom'
import clsx from 'clsx'
import {checkIsActive, KTIcon} from '../../../../helpers'

type Props = {
  to: string
  title: string
  icon?: string
  fontIcon?: string
  hasArrow?: boolean
  hasBullet?: boolean
  click?: () => void
}
type DropdownProps = {
  items: Props[]
}

const MenuItem: FC<Props> = ({
  to,
  title,
  icon,
  fontIcon,
  hasArrow = false,
  hasBullet = false,
  click = () => {},
}) => {
  const {pathname} = useLocation()

  return (
    <div className='menu-item me-lg-1'>
      <Link
        className={clsx('menu-link py-3', {
          'active menu-here': checkIsActive(pathname, to),
        })}
        to={to}
        onClick={(e) => {
          click()
        }}
      >
        {hasBullet && (
          <span className='menu-bullet'>
            <span className='bullet bullet-dot'></span>
          </span>
        )}

        {icon && (
          <span className='menu-icon'>
            <KTIcon iconName={icon} className='fs-2' />
          </span>
        )}

        {fontIcon && (
          <span className='menu-icon'>
            <i className={clsx('bi fs-3', fontIcon)}></i>
          </span>
        )}

        <span className='menu-title'>{title}</span>

        {hasArrow && <span className='menu-arrow'></span>}
      </Link>
    </div>
  )
}

const DropdownMenu: FC<DropdownProps> = ({items}) => {
  const [isOpen, setIsOpen] = useState(false)
  const [selectedItem, setSelectedItem] = useState(items[0].title) // Initialize with the first item

  const handleDropdownClick = () => {
    setIsOpen(!isOpen)
  }

  const handleItemClick = (title: string) => {
    setSelectedItem(title)
    setIsOpen(false)
  }

  return (
    <div className='dropdown'>
      <button
        className='menu-link p-3 mt-5 dropdown-toggle border-0'
        type='button'
        id='dropdownMenuButton'
        data-toggle='dropdown'
        aria-haspopup='true'
        aria-expanded={isOpen ? 'true' : 'false'}
        onClick={handleDropdownClick}
      >
        {selectedItem}
      </button>

      <div
        className={clsx('dropdown-menu', {
          show: isOpen,
        })}
        aria-labelledby='dropdownMenuButton'
      >
        {items.map((item, index) => (
          <MenuItem key={index} {...item} click={() => handleItemClick(item.title)} />
        ))}
      </div>
    </div>
  )
}

export {MenuItem, DropdownMenu}
