/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import {useIntl} from 'react-intl'
import {KTIcon} from '../../../../helpers'
import {SidebarMenuItemWithSub} from './SidebarMenuItemWithSub'
import {SidebarMenuItem} from './SidebarMenuItem'

const SidebarMenuMain = () => {
  const intl = useIntl()

  return (
    <>
      <SidebarMenuItem
        to='/dashboard'
        icon='element-11'
        title={intl.formatMessage({id: 'MENU.DASHBOARD'})}
        fontIcon='bi-app-indicator'
      />
      {/* <SidebarMenuItem to='/builder' icon='switch' title='Layout Builder' fontIcon='bi-layers' /> */}
      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>
            Machine Learning Models
          </span>
        </div>
      </div>
      {/* <SidebarMenuItemWithSub
        to='/crafted/pages'
        title='Pages'
        fontIcon='bi-archive'
        icon='element-plus'
      >
        <SidebarMenuItemWithSub to='/crafted/pages/profile' title='Profile' hasBullet={true}>
          <SidebarMenuItem to='/crafted/pages/profile/overview' title='Overview' hasBullet={true} />
          <SidebarMenuItem to='/crafted/pages/profile/projects' title='Projects' hasBullet={true} />
          <SidebarMenuItem
            to='/crafted/pages/profile/campaigns'
            title='Campaigns'
            hasBullet={true}
          />
          <SidebarMenuItem
            to='/crafted/pages/profile/documents'
            title='Documents'
            hasBullet={true}
          />
          <SidebarMenuItem
            to='/crafted/pages/profile/connections'
            title='Connections'
            hasBullet={true}
          />
        </SidebarMenuItemWithSub>

        <SidebarMenuItemWithSub to='/crafted/pages/wizards' title='Wizards' hasBullet={true}>
          <SidebarMenuItem
            to='/crafted/pages/wizards/horizontal'
            title='Horizontal'
            hasBullet={true}
          />
          <SidebarMenuItem to='/crafted/pages/wizards/vertical' title='Vertical' hasBullet={true} />
        </SidebarMenuItemWithSub>
      </SidebarMenuItemWithSub> */}
      <SidebarMenuItemWithSub
        to='/crafted/accounts'
        title='Heart Disease Prediction'
        icon='heart-circle'
        fontIcon='bi-person'
      >
        <SidebarMenuItem to='/crafted/account/overview' title='Overview' hasBullet={true} />
        <SidebarMenuItem to='/crafted/account/settings' title='Prediction' hasBullet={true} />
      </SidebarMenuItemWithSub>
      <SidebarMenuItemWithSub
        to='/crafted/loanApproval'
        title='Loan Approval Prediction'
        icon='shield-tick'
        fontIcon='bi-person'
      >
        <SidebarMenuItem to='/crafted/loanApproval/overview' title='Overview' hasBullet={true} />
        <SidebarMenuItem to='/crafted/loanApproval/settings' title='Prediction' hasBullet={true} />
      </SidebarMenuItemWithSub>
      <SidebarMenuItemWithSub
        to='/crafted/healthInsurance'
        title='Health Insurance Cross Sell Prediction'
        icon='graph'
        fontIcon='bi-person'
      >
        <SidebarMenuItem to='/crafted/healthInsurance/overview' title='Overview' hasBullet={true} />
        <SidebarMenuItem
          to='/crafted/healthInsurance/settings'
          title='Prediction'
          hasBullet={true}
        />
      </SidebarMenuItemWithSub>
      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>NLP Models</span>
        </div>
      </div>
      <SidebarMenuItemWithSub
        to='/nlp/entityExtraction'
        title='Entity Extraction'
        fontIcon='bi-sticky'
        icon='text-circle'
      >
        <SidebarMenuItem to='/nlp/entityExtraction/overview' title='All Models' hasBullet={true} />
        <SidebarMenuItem to='/nlp/entityExtraction/settings' title='Prediction' hasBullet={true} />
      </SidebarMenuItemWithSub>
      <SidebarMenuItemWithSub
        to='/nlp/textClassifier'
        title='Text Classifier'
        fontIcon='bi-sticky'
        icon='text-circle'
      >
        <SidebarMenuItem to='/nlp/textClassifier/overview' title='Overview' hasBullet={true} />
        <SidebarMenuItem to='/nlp/textClassifier/settings' title='Prediction' hasBullet={true} />
      </SidebarMenuItemWithSub>
      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>
            Regression Models
          </span>
        </div>
      </div>
      <SidebarMenuItemWithSub
        to='/regration/networkDataAnomaly'
        title='Network Data Anomaly'
        fontIcon='bi-sticky'
        icon='screen'
      >
        <SidebarMenuItem
          to='/regration/networkDataAnomaly/overview'
          title='Overview'
          hasBullet={true}
        />
        <SidebarMenuItem
          to='/regration/networkDataAnomaly/settings'
          title='Prediction'
          hasBullet={true}
        />
      </SidebarMenuItemWithSub>
      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>
            Deep Learning Models
          </span>
        </div>
      </div>
      <SidebarMenuItemWithSub
        to='/dl/objectDetection'
        title='Object Detection'
        fontIcon='bi-sticky'
        icon='magnifier'
      >
        <SidebarMenuItem to='/dl/objectDetection/overview' title='Overview' hasBullet={true} />
        <SidebarMenuItem to='/dl/objectDetection/settings' title='Prediction' hasBullet={true} />
      </SidebarMenuItemWithSub>
      <SidebarMenuItemWithSub
        to='/crafted/widgets'
        title='Widgets'
        icon='element-7'
        fontIcon='bi-layers'
      >
        <SidebarMenuItem to='/crafted/widgets/lists' title='Lists' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/statistics' title='Statistics' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/charts' title='Charts' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/mixed' title='Mixed' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/tables' title='Tables' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/feeds' title='Feeds' hasBullet={true} />
      </SidebarMenuItemWithSub>
      {/* <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>Apps</span>
        </div>
      </div>
      <SidebarMenuItemWithSub
        to='/apps/chat'
        title='Chat'
        fontIcon='bi-chat-left'
        icon='message-text-2'
      >
        <SidebarMenuItem to='/apps/chat/private-chat' title='Private Chat' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Group Chart' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Drawer Chart' hasBullet={true} />
      </SidebarMenuItemWithSub>
      <SidebarMenuItem
        to='/apps/user-management/users'
        icon='abstract-28'
        title='User management'
        fontIcon='bi-layers'
      />
      <div className='menu-item'>
        <a
          target='_blank'
          className='menu-link'
          href={process.env.REACT_APP_PREVIEW_DOCS_URL + '/docs/changelog'}
        >
          <span className='menu-icon'>
            <KTIcon iconName='code' className='fs-2' />
          </span>
          <span className='menu-title'>Changelog {process.env.REACT_APP_VERSION}</span>
        </a>
      </div> */}
    </>
  )
}

export {SidebarMenuMain}
